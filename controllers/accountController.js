var express = require('express');
var crypto = require('crypto');

var account = require('../models/account');
var product = require('../models/product');
var category = require('../models/category');
var restrict = require('../middle-wares/restrict');
var restrictadmin = require('../middle-wares/restrictadmin');
var captchapng = require('captchapng');
var mailService = require('../services/mail');
var moment = require('moment');

var router = express.Router();

var captchaInfo = function(){
	var numeric = parseInt(Math.random()*9000+1000);
    var p = new captchapng(80,30,numeric); // width,height,numeric captcha
    p.color(115, 95, 197, 100);  // First color: background (red, green, blue, alpha)
    p.color(30, 104, 21, 255); // Second color: paint (red, green, blue, alpha)
    var img = p.getBase64();
    var imgbase64 = new Buffer(img,'base64');
    var data = {
    	numeric: numeric,
    	imgbase64: imgbase64
    }
    return data;
}

var randomString = function(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

router.get('/sellingproducts', restrict, function(req, res) {
	var userId = req.session.user.id;
	product.sellingProducts(userId)
		.then(function(products) {
			res.render('account/sellingproducts', {
				layoutModels: res.locals.layoutModels,
				products: products
			});
		})
});

router.get('/soldproducts', restrict, function(req, res){
	var userId = req.session.user.id;
	product.soldProducts(userId)
		.then(function(products){
			res.render('account/soldproducts', {
				layoutModels: res.locals.layoutModels,
				products: products
			})
		})
});

router.get('/register', function(req, res) {
	
	var captchaData = captchaInfo();
	var captchaImg = captchaData.imgbase64;
	var valicode = new Buffer(captchaImg).toString('base64');
	var captchaCode = captchaData.numeric;
	res.render('account/register', {
		captchaCode: captchaCode,
		captchaImg: valicode
	});
});

router.post('/register', function(req, res) {
	var hashPass = crypto.createHash('md5').update(req.body.password).digest('hex');

	var entity = {
        username: req.body.username,
        password: hashPass,
        fullname: req.body.fullname,
        email: req.body.email,
        address: req.body.address,
        permission: 0
    };

    account.checkAccount(entity)
    	.then(function(user)
    	{
    		if(user === null)
    		{
    			account.insert(entity)
			    	.then(function(insertId) {
			    		res.render('account/register', {
			    			// layoutModels: res.locals.layoutModels,
			    			showError: true,
			    			errorMsg: 'Đăng ký thành công',
			    		})
			    	});
    		}
    		else if(user.email == req.body.email)
    		{
    			res.render('account/register', {
			    			// layoutModels: res.locals.layoutModels,
			    			showError: true,
			    			errorMsg: 'Email đã tồn tại!!!',
			    		})
    		}
    		else if(user.username == req.body.username)
    		{
    			res.render('account/register', {
			    			// layoutModels: res.locals.layoutModels,
			    			showError: true,
			    			errorMsg: 'Tên đăng nhập đã tồn tại!!!',
			    		})
    		}
    	})

})

router.get('/login', function(req, res) {
	if(req.session.isLogged === true) {
		res.redirect('/');
	} else {
		res.render('account/login');
	}
});

router.post('/login', function(req, res) {
	var hashPass = crypto.createHash('md5').update(req.body.password).digest('hex');
	var entity = {
		username: req.body.username,
		password: hashPass
	}

	var remember = req.body.remember ? true : false;
	console.log(entity);

	account.login(entity)
		.then(function(user) {
			if(user === null) {
				res.render('account/login', {
					showError: true,
					errorMsg: 'Tài khoản và mật khẩu không đúng. Xin kiểm tra lại!'
				});
			} else {
				req.session.isLogged = true;
				req.session.user = user;
				req.session.cart = [];
				req.session.isAdmin = (req.session.user.permission_id == 1);
				req.session.isGuest = (req.session.user.permission_id == 2);

				// if remember
				if(remember === true) {
					var hour = 1000 * 60 * 60 * 24;
					req.session.cookie.expires = new Date(Date.now() + hour);
					req.session.cookie.maxAge = hour;
				}

				var url = '/';
				if(req.query.retUrl) {
					url = req.query.retUrl;
				}
				res.redirect(url);
			}
		});
});

router.post('/logout', restrict, function(req, res) {
	req.session.isLogged = false;
	req.session.user = null;
	req.session.cart = null;
	req.session.cookie.expires = new Date(Date.now() - 1000);
    res.redirect(req.headers.referer);
});

router.get('/profile', restrict, function(req, res) {

	account.getUser(req.session.user.id)
		.then(function(user) {
			console.log(user);
			if(user) {
				res.render('account/profile', {
					layoutModels: res.locals.layoutModels,
					user: user,
					isWaiting: user.request_permission == 1,
				});
			}
		})
});

router.get('/update', restrict, function(req, res) {
	res.render('account/update', {
		layoutModels: res.locals.layoutModels
	});
});

router.post('/update', restrict, function(req, res) {

	var curPass = req.body.curPassword;
	if(curPass == '') {
		res.render('account/update', {
			showError: true,
			errorMsg: 'Bạn phải nhập mật khẩu hiện tại'
		});
		return false;
	}

	var hashCurPass = crypto.createHash('md5').update(req.body.curPassword).digest('hex');

	var entityCheckUser = {
		id: req.session.user.id,
		password: hashCurPass
	}

	account.checkPassword(entityCheckUser)
		.then(function(checkUser) {
			if(checkUser === null) {
				res.render('account/update', {
					showError: true,
					errorMsg: 'Mật khẩu hiện tại của bạn không đúng!'
				});
				return false;
			} else {
				var hashPass = (req.body.password != '') ? crypto.createHash('md5').update(req.body.password).digest('hex') : '';

				var entity = {
					fullname: req.body.fullname,
					email: req.body.email,
					password: hashPass,
					id: req.session.user.id
				}

				account.update(entity)
					.then(function(user) {
						if(user === null) {
							res.render('account/login', {
								showError: true,
								errorMsg: 'Thông tin có lỗi! Chưa cập nhật được'
							});
						} else {
							req.session.isLogged = true;
							req.session.user = user;
							// req.session.cart = [];

							res.redirect('/account/profile');
						}
					})
			}
		})
});

router.get('/products', restrict, function(req, res) {
	res.render('account/products', {
		layoutModels: res.locals.layoutModels
	});
});

router.get('/favorite', restrict, function(req, res) {

	product.favorite(req.session.user.id)
		.then(function(products) {
			if(products === null) {
				res.render('account/favorite' , {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Không có sản phẩm yêu thích'
				})
			} else {
				res.render('account/favorite', {
					layoutModels: res.locals.layoutModels,
					products: products
				})
			}
		});
})

router.get('/admin', restrictadmin, function(req, res) {

	account.getAll()
		.then(function(users) {
			if(users.length > 0) {
				res.render('account/admin', {
					layoutModels: res.locals.layoutModels,
					users: users
				});
			}
		})
});

router.post('/delete', restrictadmin, function(req, res) {

	var id = req.body.id;

	account.deactive(id)
		.then(function(row) {
			if(row !== null && row > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		});
});

router.post('/resetpass', restrictadmin, function(req, res) {

	var id 		= req.body.id;
	var newPass = randomString(10);
	var newPassHash = crypto.createHash('md5').update(newPass).digest('hex');

	account.resetPass(id, newPassHash)
		.then(function(changedRows) {
			if(changedRows !== null && changedRows > 0) {
				var content = 'Thông báo: mật khẩu của bạn đã được reset.\n';
				content += ('Mật khẩu mới: ' + newPass);
				mailService.send(req.body.email, 'Reset mật khẩu', content);
				res.send(true);
			} else {
				res.send(false);
			}
		})
});

router.get('/admin/category', restrictadmin, function(req, res) {

	res.render('account/category/index', {
		layoutModels: res.locals.layoutModels
	});
});

router.get('/admin/category/create', restrictadmin, function(req, res) {

	res.render('account/category/create', {
		layoutModels: res.locals.layoutModels
	});
});

router.post('/admin/category/create', restrictadmin, function(req, res) {
	var entity = {
		name: req.body.name,
		slug: req.body.slug,
	}

	category.create(entity)
		.then(function(insertId) {
			if(insertId !== null && insertId > 0) {
				res.render('account/category', {
					layoutModels: res.locals.layoutModels,
					showSuccess: true,
					successMsg: 'Đã thêm thành công thể loại ' + entity.name
				});
			}
		})
});

router.get('/admin/category/detail/:id', restrictadmin, function(req, res) {

	var id = req.params.id;

	category.loadDetail(id)
		.then(function(category) {
			res.render('account/category/edit', {
				category: category,
				layoutModels: res.locals.layoutModels
			});
		})
});

router.post('/admin/category/detail/:id', restrictadmin, function(req, res) {
	var entity = {
		name: req.body.name,
		slug: req.body.slug,
		id: req.params.id
	}

	category.edit(entity)
		.then(function(changedRows) {
			if(changedRows !== null && changedRows > 0) {
				res.redirect('/account/admin/category');
			}
		})
});

router.post('/admin/category/delete', restrictadmin, function(req, res) {

	var id = req.body.id;

	category.delete(id)
		.then(function(affectedRows) {
			if(affectedRows !== null && affectedRows > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		})
});

router.get('/upgradeaccount', restrict, function(req, res) {

	var userId = req.session.user.id;

	account.upgrade(userId)
		.then(function(changedRows) {
			if(changedRows !== null && changedRows > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		})
});

router.get('/user/waitingupgrade', restrictadmin, function(req, res) {

	account.getUpgradeAccount()
		.then(function(users) {
			res.render('account/user/waitingupgrade', {
				layoutModels: res.locals.layoutModels,
				users: users
			});
		});
})

router.post('/user/waitingupgrade', restrictadmin, function(req, res) {

	var id = req.body.id;

	account.confirmUpgradeAccount(id)
		.then(function(changedRows) {
			if(changedRows !== null && changedRows > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		});
});

router.get('/admin/product', restrictadmin, function(req, res) {

	product.getAll()
		.then(function(products) {
			if(products === null) {
				res.render('account/product/index', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Không có sản phẩm',
				})
			} else {
				res.render('account/product/index', {
					layoutModels: res.locals.layoutModels,
					products: products
				})
			}
		})
});

router.post('/admin/product/confirmauction', restrictadmin, function(req, res) {

	var id = req.body.id;

	var expiredDate = moment(Date.now() + 7 * 24 * 3600 * 1000).format('YYYY-MM-DD HH:mm:ss');

	product.confirmAuction(id, expiredDate)
		.then(function(changedRows) {
			if(changedRows !== null && changedRows > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		})
});

router.get('/admin/product/biding', restrict, function(req, res) {

	var userId = req.session.user.id;
	product.loadBiding(userId)
		.then(function(products) {
			if(products.length > 0) {
				res.render('account/product/biding', {
					layoutModels: res.locals.layoutModels,
					products: products
				});
			} else {
				res.render('account/product/biding', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Không có sản phẩm đang đấu giá',
				});
			}
		})
});

router.get('/admin/product/won', restrict, function(req, res) {

	var userId = req.session.user.id;
	product.loadWonBiding(userId)
		.then(function(products) {
			if(products.length > 0) {
				res.render('account/product/won', {
					layoutModels: res.locals.layoutModels,
					products: products
				});
			} else {
				res.render('account/product/won', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Không có sản phẩm đã thắng',
				});
			}
		})
});

module.exports = router;