var express = require('express');
var crypto = require('crypto');

var product = require('../models/product');
var category = require('../models/category');

var router = express.Router();

router.get('/:slug', function(req, res) {

	var productPerPage = 8;
	var curPage = req.query.page ? req.query.page : 1;
	var offset = (curPage - 1) * productPerPage;

	category.loadPageByCat(req.params.slug, productPerPage, offset)
		.then(function(result) {

			var numberOfPages = result.total / productPerPage;
            if (result.total % productPerPage > 0) {
                numberOfPages++;
            }

            var pages = [];
            for (var i = 1; i <= numberOfPages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }

			res.render('category/bySlug', {
				layoutModels: res.locals.layoutModels,
				products: result.list,
				isEmpty: result.total === 0,
				total: result.total,
				pages: pages,
				categoryName: result.list[0].name
			});
		});
});

module.exports = router;