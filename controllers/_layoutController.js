var Q = require('q');
var category = require('../models/category');
// var cart = require('../models/cart');

module.exports = function(req, res, next) {

    Q.all([
        category.loadAll()
    ]).spread(function(catList) {
        res.locals.layoutModels = {
            categories: catList,
            isLogged: req.session.isLogged,
            curUser: req.session.user,
            isAdmin: req.session.isAdmin,
            isGuest: req.session.isGuest,
            // cartSumQ: cart.sumQ(req.session.cart)
        };

        next();
    });
};