var express = require('express');
var product = require('../models/product');
var account = require('../models/account');
var mailService = require('../services/mail');

var router = express.Router();

router.get('/', function(req, res) {
	product.loadTop()
		.then(function(result) {
			res.render('home/index', {
				layoutModels: res.locals.layoutModels,
				topCount: result.topCount,
				topPrice: result.topPrice,
				topExpiring: result.topExpiring
			});
		});
});

router.get('/review/:id', function(req, res) {
	// mailService.send('dpsanh2348@gmail.com', 'xin chao', 'ok');
	var id = req.params.id;

	account.getUser(id)
		.then(function(user) {
			if(user) {
				var dgKT = user.review_count - user.review_point;
				var perComplete = (user.review_point / user.review_count)*1.0*100;
				if(user.review_count==0)
				{
					perComplete=0;
				}
				res.render('home/review', {
					layoutModels: res.locals.layoutModels,
					user: user,
					dgKT: dgKT,
					perComplete: perComplete
				})
			} else {
				res.render('home/review', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Khong tim thay user'
				})
			}
		})
	
});


module.exports = router;