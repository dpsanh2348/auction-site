var express = require('express');
var crypto = require('crypto');


var product = require('../models/product');
var category = require('../models/category');
var account = require('../models/account');
var restrict = require('../middle-wares/restrict');

var mailService = require('../services/mail');

var router = express.Router();

router.get('/detail/:id', function(req, res) {

	product.loadDetail(req.params.id)
		.then(function(result) {
			if(result) {
				isSeeReviewSeller = false;
				canAuction = req.session.isLogged;
				if(typeof req.session.isLogged !== 'undefined' && req.session.isLogged) {
					for(var i in result.auctions) {
						if(result.auctions[i].user_id == req.session.user.id) {
							isSeeReviewSeller = true;
						}
					}
					for(var j in result.kickers) {
						if(result.kickers[j].user_id == req.session.user.id) {
							canAuction = false;
						}
					}
				}
				res.render('product/detail', {
					layoutModels: res.locals.layoutModels,
					product: result.product,
					user: result.user,
					userAuction: result.userAuction,
					auctions: result.auctions,
					isSeller: (typeof req.session.user !== 'undefined') ? (req.session.user.id == result.product.user_id) : false,
					isSeeReviewSeller: isSeeReviewSeller,
					descriptions: result.descriptions,
					images: result.images,
					canAuction: canAuction,
				});
			} else {
				res.redirect('/');
			}
		});
});

router.get('/search', function(req, res) {

	// if(typeof req.params.key === 'undefined' || req.params.key == '') {
	// 	res.redirect('/');
	// }

	var searchKey = req.query.value;
	var searchType = req.query.type;
	var searchOrder = req.query.order ? req.query.order : 0;

	var productPerPage = 8;
	var curPage = req.query.page ? req.query.page : 1;
	var offset = (curPage - 1) * productPerPage;

	product.loadSearch(searchKey, searchType, searchOrder, productPerPage, offset)
		.then(function(result) {

			var numberOfPages = result.total / productPerPage;
            if (result.total % productPerPage > 0) {
                numberOfPages++;
            }

            var pages = [];
            for (var i = 1; i <= numberOfPages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }
			var data = {
				layoutModels: res.locals.layoutModels,
				products: result.list,
				isEmpty: result.total === 0,
				errorMsg: 'Không tìm thấy sản phẩm có từ khóa "' + searchKey + '"',
				total: result.total,
				pages: pages,
				type: searchType,
				value: searchKey,
				searchOrder: searchOrder,
			}

			res.render('product/search', data);
		})
});

router.get('/create', restrict, function(req, res) {
	res.render('product/create', {
		layoutModels: res.locals.layoutModels,
	})
});

router.post('/create', restrict, function(req, res) {

	var entity = {
		productName: req.body.productName,
		productCategory: req.body.productCategory,
		productStartPrice: req.body.productStartPrice,
		productGapPrice: req.body.productGapPrice,
		productBuyNowPrice: req.body.productBuyNowPrice,
		productDescription: req.body.productDescription,
		userId: req.session.user.user_id,
		productImage: "datejust-rolex.jpg"
	}

	product.create(entity)
		.then(function(insertId) {
			res.render('product/create', {
				layoutModels: res.locals.layoutModels,
				showSuccess: true,
				successMsg: 'Đã tạo thành công sản phẩm.'
			});
		});
});

router.post('/like', restrict, function(req, res) {

	var entity = {
		productId: req.body.id,
		userId: req.session.user.id
	}

	account.likeProduct(entity)
		.then(function(changedRow) {
			if(changedRow !== null && changedRow > 0) {
				res.send(true);
			} else {
				res.send(false);
			}
		});
})

router.post('/auction', function(req, res) {

	var entity = {
		productId: req.body.productId,
		priceAuction: req.body.priceAuction,
		countAuction: req.body.countAuction,
		userAuction: req.session.user.id,
		productName: req.body.productName,
	};

	account.getUser(req.session.user.id)
		.then(function(user) {

			if(parseInt(user.review_count) == 0) {
				var percent = 1;
			} else {
				var percent = (parseInt(user.review_point) / parseInt(user.review_count))*1.0;
			}
			if(percent > 0.8) {
				product.auction(entity)
					.then(function(row) {
						if(row === null) {
							res.send(false);
						} else {
							var content = 'Chúc mừng bạn đã đấu giá thành công sản phẩm ' + entity.productName;
							content += ' - giá: ' + entity.priceAuction;
							mailService.send(req.session.user.email, 'Đấu giá thành công', content);
							res.send(true);
						}
					});
			} else {
				res.send(false);
			}
		});
});

router.get('/update/:id', restrict, function(req, res) {

	product.loadDescription(req.session.user.id, req.params.id)
		.then(function(result) {
			if(result === null) {
				res.render('product/update', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Bạn không có quyền truy cập vào sản phẩm này'
				})
			} else {
				res.render('product/update', {
					layoutModels: res.locals.layoutModels,
					bigDescription: result.bigDescription,
					smallDescriptions: result.smallDescriptions,
				})
			}
		});
});

router.post('/update/:id', restrict, function(req, res) {

	var entity = {
		productId: req.params.id,
		productDescription: req.body.productDescription,
		userId: req.session.user.id,
	}

	product.addMoreDescription(entity)
		.then(function(insertId) {
			if(insertId > 0) {
				res.redirect('/product/detail/' + entity.productId);
			} else {
				res.render('product/update', {
					layoutModels: res.locals.layoutModels,
					showError: true,
					errorMsg: 'Chưa lưu được. Xin thử lại sau',
				})
			}
		});
});

router.post('/kick', function(req, res) {
	var productId = req.body.productId;
	var userId = req.body.userId;

	product.kick(productId, userId)
		.then(function(insertId) {
			if(insertId > 0) {
				var content = 'Bạn đã bị loại khỏi danh sách đấu giá của sản phẩm ' + req.body.productName;
				mailService.send(req.body.email, 'Bị loại khỏi danh sách đấu giá', content);
				res.send(true);
			} else {
				res.send(false);
			}
		})
})

module.exports = router;