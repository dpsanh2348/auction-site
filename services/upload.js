var multer = require('multer');



exports.upload = function(serverReq, serverRes, imageNameInput, uploadDir = '../public/img/products/') {

	var storage = multer.diskStorage({
		destination: function (req, file, callback) {
		    callback(null, uploadDir);
		},
		filename: function (req, file, callback) {
			callback(null, file.fieldname + '-' + Date.now());
		}
	});

	var upload = multer({ storage : storage }).array(imageNameInput, 5);

	upload(serverReq, serverRes, function(err) {
		if(err) {
			console.log('upload error');
		} else {
			console.log('successful');
		}
	})
}