var mysql = require('mysql'),
	q = require('q');

var _HOST = '127.0.0.1',
	_USER = 'root',
	_PWD = '',
	_DB = 'auction_site';

exports.load = function(sql) {
	var d = q.defer();

	var connection = mysql.createConnection({
		host: _HOST,
		user: _USER,
		password: _PWD,
		database: _DB
	});

	connection.connect();

	connection.query(sql, function(err, rows, fields) {
		if(err)
			d.reject(err);

		d.resolve(rows);
	});

	connection.end();

	return d.promise;
}

exports.loadOne = function(sql) {
	var d = q.defer();

	var connection = mysql.createConnection({
		host: _HOST,
		user: _USER,
		password: _PWD,
		database: _DB
	});

	connection.connect();

	connection.query(sql, function(err, rows, fields) {
		if(err)
			d.reject(err);

		d.resolve(rows[0]);
	});

	connection.end();

	return d.promise;
}

exports.insert = function(sql) {
	var d = q.defer();

	var connection = mysql.createConnection({
		host: _HOST,
		user: _USER,
		password: _PWD,
		database: _DB
	});

	connection.connect();

	connection.query(sql, function(err, value) {
		if(err)
			d.reject(err);

		d.resolve(value.insertId);
	});

	connection.end();

	return d.promise;
}

exports.update = function(sql) {
	var d = q.defer();

	var connection = mysql.createConnection({
		host: _HOST,
		user: _USER,
		password: _PWD,
		database: _DB
	});

	connection.connect();

	connection.query(sql, function(err, value) {
		if(err)
			d.reject(err);

		d.resolve(value.changedRows);
	});

	connection.end();

	return d.promise;
}

exports.delete = function(sql) {
	var d = q.defer();

	var connection = mysql.createConnection({
		host: _HOST,
		user: _USER,
		password: _PWD,
		database: _DB
	});

	connection.connect();

	connection.query(sql, function(err, value) {
		if(err)
			d.reject(err);

		d.resolve(value.affectedRows);
	});

	connection.end();

	return d.promise;
}