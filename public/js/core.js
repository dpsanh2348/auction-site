$(function() {
	var btnSearch = $('.btnSearch');
	var txtSearch = $('#txtSearch');


	$('#logoutLink').on('click', function () {
        $('#frmLogout').submit();
    });

	btnSearch.on('click', function() {
		var type = $(this).data('type');
		console.log(type);
		// window.location.replace('//localhost:3000/product/search?type=' + type + '&value=' + txtSearch.val());
		location.href = '/product/search?type=' + type + '&value=' + txtSearch.val();
	});

	txtSearch.keypress(function(e) {
		if(e.which == 13 || e.keyCode == 13) {
			btnSearch.eq(0).click();
		}
	})
})