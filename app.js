var express = require('express'),
	session = require('express-session'),
	MySQLStore = require('express-mysql-session')(session);
	handlebars = require('express-handlebars'),
	handlebars_sections = require('express-handlebars-sections'),
	bodyParser = require('body-parser'),
	morgan = require('morgan'),
	path = require('path'),
	wnumb = require('wnumb'),
	// handleLayout = require('./middle-wares/handleLayout'),
	_layoutController = require('./controllers/_layoutController'),
	homeController = require('./controllers/homeController'),
	accountController = require('./controllers/accountController'),
	productController = require('./controllers/productController'),
	categoryController = require('./controllers/categoryController');

var moment = require('moment');
var wnumb = require('wnumb');

var app = express();

var port = 3000;


app.use(morgan('dev'));

app.engine('hbs', handlebars({
	extname: 'hbs',
	defaultLayout: 'main',
	layoutsDir: 'views/_layouts/',
	partialsDir: 'views/_partials/',
	helpers: {
		section: handlebars_sections(),
		getName: function(fullname) {
			var part = fullname.split(' ');
			return part[part.length - 1];
		},
		now: function() {
            return moment().format('D/M/YYYY - HH:mm');
        },
        friendlyTime: function(time) {
        	return moment(time).format('DD/MM/YYYY - HH:mm:ss');
        },
        formatNumber: function(n) {
            var nf = wnumb({
                thousand: ','
            });
            return nf.to(n);
        },
        encodeName: function(name) {
        	var char = name.split('');
        	var encodedName = '';
        	for(var i = 0; i < char.length; i++) {
        		if((i+1) % 2 == 0) {
        			encodedName += '*';
        		} else {
        			encodedName += char[i];
        		}
        	}
        	return encodedName;
        },
        showAuctionUser: function(name, price) {
        	if(typeof name == 'number' || name == 0 || price == 0) {
        		return 'Chưa có người đấu giá';
        	} else {
        		var char = name.split('');
	        	var encodedName = '';
	        	var nf = wnumb({
	                thousand: ','
	            });
	        	for(var i = 0; i < char.length; i++) {
	        		if((i+1) % 2 == 0) {
	        			encodedName += '*';
	        		} else {
	        			encodedName += char[i];
	        		}
	        	}
	        	return encodedName + ' - ' + nf.to(price);
        	}
        },
        expiredTime: function(time) {
        	var end = moment(time);
        	var now = moment().format('D/M/YYYY - HH:mm:ss');
        	var duration = moment.duration(end.diff(now));
        	var hours = duration.asHours();
        	return hours + 'giờ';
        },
        recommendPrice: function(present, gap) {
        	var nf = wnumb({
                thousand: ','
            });
        	return nf.to(parseInt(present) + parseInt(gap));
        },
        convertIndexToNo: function(index) {
        	return parseInt(index) + 1;
        },
        if_eq: function(a, b, opts) {
        	if(a == b) {
        		return opts.fn(this);
        	} else {
        		return opts.inverse(this);
        	}
        },
        ifInArray: function(char, arr, opts) {
        	if(typeof arr == 'string')
        		arr = arr.split(',');

        	if(arr.indexOf(char.toString()) !== -1) {
        		return opts.fn(this);
        	} else {
        		return opts.inverse(this);
        	}
        },
        selectedOption: function(serverOpt, clientOpt) {
        	if(serverOpt == clientOpt) {
        		return 'selected';
        	} else {
        		return '';
        	}
        },
        isNewProduct: function(time, opts) {
        	var start = moment(time);
        	var now = moment();
        	var duration = moment.duration(now.diff(start));
        	var hours = duration.asHours();
        	if(parseFloat(hours) <= 1) {
        		return opts.fn(this);
        	} else {
        		return opts.inverse(this);
        	}
        }
	}
}));

app.set('view engine', 'hbs');
app.set('/view' )

app.use(express.static(
	path.resolve(__dirname, 'public')
));

app.use('/node_modules', express.static(__dirname + '/node_modules'));

// app.use(express.bodyParser({ keepExtensions: true, uploadDir: "/img/products" }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

app.use(session({
	secret: 'sanhdp',
	resave: false,
	saveUninitialized: true,
	store: new MySQLStore({
        host: '127.0.0.1',
        port: 3306,
        user: 'root',
        password: '',
        database: 'auction_site',
        createDatabaseTable: true,
        schema: {
            tableName: 'sessions',
            columnNames: {
                session_id: 'session_id',
                expires: 'expires',
                data: 'data'
            }
        }
    }),
}));


// app.use(handleLayout);
app.use(_layoutController);
app.use('/', homeController);
app.use('/account', accountController);
app.use('/category', categoryController);
app.use('/product', productController);


app.use(function(req, res) {
	res.statusCode = 404;
	res.end('404! Page not found');
});



app.listen(port, function() {
	console.log('Connected to port ' + port);
});