var Q = require('q');
var mustache = require('mustache');

var db = require('../fn/db.js');

exports.loadAll = function(entity) {
	var deferred = Q.defer();

	var sql = mustache.render(
		'SELECT * FROM categories'
	);

	db.load(sql).then(function(rows) {
		deferred.resolve(rows);
	});

	return deferred.promise;
}

exports.loadDetail = function(id) {
	var deferred = Q.defer();

	var sql = mustache.render(
		'SELECT * FROM categories WHERE id = ' + id
	);

	db.loadOne(sql).then(function(row) {
		deferred.resolve(row);
	});

	return deferred.promise;
}

exports.loadPageByCat = function(slug, limit, offset) {
	var deferred = Q.defer();

	var promises = [];

	var data = {
		slug: slug,
		limit: limit,
		offset: offset
	};

	var sqlCount = mustache.render(
		'SELECT COUNT(*) AS total FROM products AS p JOIN categories AS c ON p.category_id=c.id WHERE c.slug = "{{slug}}"', data
	);
	promises.push(db.load(sqlCount));

	var sql = mustache.render(
		'SELECT * FROM products AS p JOIN categories AS c ON p.category_id = c.id LEFT JOIN users as u ON p.user_auction=u.user_id WHERE c.slug = "{{ slug }}" limit {{ limit }} offset {{ offset }}', data
	);
	promises.push(db.load(sql));

	Q.all(promises).spread(function(totalRow, rows) {
		var result = {
			total: totalRow[0].total,
			list: rows
		};
		deferred.resolve(result);
	})

	return deferred.promise;
}

exports.create = function(entity) {
	var deferred =  Q.defer();

	var sql = mustache.render(
		'INSERT INTO categories (name, slug) VALUES ("{{name}}", "{{slug}}")',
		entity
	);

	db.insert(sql).then(function(insertId) {
		if(insertId > 0) {
			deferred.resolve(insertId);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.edit = function(entity) {

	var deferred = Q.defer();

	var sql = mustache.render(
		'UPDATE categories SET name = "{{name}}", slug = "{{slug}}" WHERE id = "{{id}}"',
		entity
	);

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.delete = function(id) {
	var deferred = Q.defer();

	var sql = 'SELECT * FROM products WHERE category_id=' + id;

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(null);
		} else {
			var sqlCat = 'DELETE FROM categories WHERE id = ' + id;
			db.delete(sqlCat).then(function(affectedRows) {
				console.log(affectedRows);
				if(affectedRows > 0) {
					deferred.resolve(affectedRows);
				} else {
					deferred.resolve(null);
				}
			})
		}
	});

	return deferred.promise;
}