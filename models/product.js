var Q = require('q');
var mustache = require('mustache');

var db = require('../fn/db.js');

exports.sellingProducts = function(userID){
	var deferred = Q.defer();
	var sqlProductOfUser = 'SELECT * FROM products WHERE user_id = ' + userID + ' AND expired_date > NOW() AND is_active=1 AND price_present <= price_buy_now';
	db.load(sqlProductOfUser).then(function(rows) {
		if(rows) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	})
	return deferred.promise;
}
exports.soldProducts = function(userID){
	var deferred = Q.defer();
	var sqlProductOfUser = 'SELECT * FROM products WHERE user_id = ' + userID + ' AND (price_present >= price_buy_now OR (expired_date < NOW() AND user_auction IS NOT null))';
	db.load(sqlProductOfUser).then(function(rows){
		if(rows) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	})
	return deferred.promise;
}


exports.loadTop = function() {
	var deferred = Q.defer();

	var promises = [];

	var sqlTopCountAuction = 'SELECT * FROM products WHERE is_active=1 ORDER BY count_auction DESC LIMIT 0,5';
	promises.push(db.load(sqlTopCountAuction));

	var sqlTopPrice = 'SELECT * FROM products WHERE is_active=1 AND expired_date < NOW() ORDER BY price DESC LIMIT 0,5';
	promises.push(db.load(sqlTopPrice));

	var sqlTopExpiring = 'SELECT * FROM products WHERE is_active=1 AND expired_date < NOW() ORDER BY expired_date DESC LIMIT 0,5';
	promises.push(db.load(sqlTopExpiring));

	Q.all(promises).spread(function(topCount, topPrice, topExpiring) {
		var result = {
			topCount: topCount,
			topPrice: topPrice,
			topExpiring: topExpiring
		}
		deferred.resolve(result);
	});

	return deferred.promise;
}

exports.loadDescription = function(userId, productId) {
	var deferred = Q.defer();

	var sql = 'SELECT description FROM products WHERE user_id="' + userId + '" AND product_id="' + productId + '"';
	db.loadOne(sql).then(function(row) {
		console.log(row);
		if(row) {
			var sqlMuch = 'SELECT * FROM product_description WHERE product_id = ' + productId;
			db.load(sqlMuch).then(function(rows) {
				var data = {
					bigDescription: row.description,
					smallDescriptions: rows
				}

				deferred.resolve(data);
			})
		} else {
			deferred.resolve(null);
		}
	})

	return deferred.promise;
}

exports.loadDetail = function(id) {

	var deferred = Q.defer();

	var sqlProduct = 'SELECT * FROM products WHERE product_id = ' + id;

	db.loadOne(sqlProduct).then(function(row) {
		if(row) {
			var promises = [];

			var sqlUser = 'SELECT * FROM users WHERE user_id = ' + row.user_id;
			promises.push(db.loadOne(sqlUser));

			var sqlUserAuction = 'SELECT * FROM users WHERE user_id = ' + row.user_auction;
			promises.push(db.loadOne(sqlUserAuction));

			var sqlBidLog = 'SELECT * FROM auction as a left join users as u on a.user_auction=u.user_id WHERE product_id = ' + id;
			promises.push(db.load(sqlBidLog));

			var sqlDescriptions = 'SELECT * FROM product_description WHERE product_id = ' + row.product_id;
			promises.push(db.load(sqlDescriptions));

			var sqlImages = 'SELECT * FROM product_images WHERE product_id = ' + row.product_id;
			promises.push(db.load(sqlImages));

			var sqlKick = 'SELECT * FROM kick WHERE product_id = ' + row.product_id;
			promises.push(db.load(sqlKick));

			Q.all(promises).spread(function(user, userAuction, auctions, descriptions, images, kickers) {
				var data = {
					product: row,
					user: user,
					userAuction: userAuction,
					auctions: auctions,
					descriptions: descriptions,
					images: images,
					kickers: kickers,
				};

				deferred.resolve(data);
			});
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.loadSearch = function(searchKey, searchType, searchOrder, limit, offset) {

	var deferred = Q.defer();

	var data = {
		searchKey: searchKey,
		limit: limit,
		offset: offset
	}

	var orderArr = ['expired_date DESC', 'expired_date ASC', 'price_present ASC', 'price_present DESC'];
	var orderSql = orderArr[searchOrder];
	console.log(orderSql);

	var promises = [];

	if(searchType == 1) {
		// product name
		var sqlCount = mustache.render(
			'SELECT count(*) as total FROM products WHERE product_name LIKE "%{{searchKey}}%"',
			data
		);
		var sql = mustache.render(
			'SELECT * FROM products WHERE product_name LIKE "%{{searchKey}}%" order by ' + orderSql + ' limit {{ limit }} offset {{ offset }}',
			data
		);
		console.log(sql);
	} else if(searchType == 2) {
		// category name
		var sqlCount = mustache.render(
			'SELECT count(*) as total FROM products AS p JOIN categories AS c ON p.category_id=c.id WHERE c.name LIKE "%{{searchKey}}%"',
			data
		);
		var sql = mustache.render(
			'SELECT * FROM products AS p JOIN categories AS c ON p.category_id=c.id WHERE c.name LIKE "%{{searchKey}}%" order by p.' + orderSql + ' limit {{ limit }} offset {{ offset }}',
			data
		);
	} else if(searchType == 3) {
		// product description
		var sqlCount = mustache.render(
			'SELECT count(*) as total FROM products WHERE description LIKE "%{{searchKey}}%"',
			data
		);
		var sql = mustache.render(
			'SELECT * FROM products WHERE description LIKE "%{{searchKey}}%" order by ' + orderSql + ' limit {{ limit }} offset {{ offset }}',
			data
		);
	}

	promises.push(db.load(sqlCount));
	promises.push(db.load(sql));

	Q.all(promises).spread(function(totalRow, rows) {
		var result = {
			total: totalRow[0].total,
			list: rows
		};
		console.log(result);
		deferred.resolve(result);
	});

	return deferred.promise;
}

exports.create = function(entity) {

	var deferred = Q.defer();

	var sql = mustache.render(
		'INSERT INTO products (product_name, price, price_present, category_id, description, price_buy_now, user_id, price_gap, image, created_at) VALUES ("{{productName}}", "{{productStartPrice}}", "{{productStartPrice}}", "{{productCategory}}", "{{{productDescription}}}", "{{productPriceBuyNow}}", "{{userId}}", "{{productGapPrice}}", "{{productImage}}", NOW())',
		entity
	);

	db.insert(sql).then(function(insertId) {
		deferred.resolve(insertId);
	});

	return deferred.promise;
}

exports.auction = function(entity) {

	var deferred = Q.defer();

	var promises = [];

	var sql = mustache.render(
		'UPDATE products SET user_auction = "{{userAuction}}", price_present = "{{priceAuction}}", count_auction = "{{countAuction}}" WHERE product_id = "{{productId}}"',
		entity
	);
	promises.push(db.update(sql));

	var sqlLog = mustache.render(
		'INSERT INTO auction (product_id, user_auction, price_auction) VALUES ("{{productId}}", "{{userAuction}}", "{{priceAuction}}")',
		entity
	);
	promises.push(db.insert(sqlLog));

	Q.all(promises).spread(function(changedRows, insertId) {
		if(changedRows > 0 && insertId > 0) {
			var row = changedRows[0];
			deferred.resolve(row);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.favorite = function(userId) {

	var deferred = Q.defer();

	var sql = 'SELECT user_id, favorite_products FROM users WHERE user_id = ' + userId;

	db.loadOne(sql).then(function(row) {
		if(row) {
			var favoriteProducts = row.favorite_products;

			var sqlProducts = 'SELECT * FROM products as p left join users as u on p.user_id=u.user_id WHERE product_id IN (' + favoriteProducts + ')';
			db.load(sqlProducts).then(function(rows) {
				if(rows) {
					deferred.resolve(rows);
				} else {
					deferred.resolve(null);
				}
			})
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.getAll = function() {

	var deferred = Q.defer();

	var sql = 'SELECT *, p.is_active p_is_active FROM products as p left join users as u on p.user_id=u.user_id ORDER BY p.product_id DESC';

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.confirmAuction = function(id, expiredDate) {
	var deferred = Q.defer();

	var sql = 'UPDATE products SET is_active=1, expired_date="' + expiredDate + '", created_at = NOW() WHERE product_id=' + id;

	db.update(sql).then(function(changedRows) {
		if(changedRows> 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.loadBiding = function(userId) {
	var deferred = Q.defer();

	var sql = 'SELECT *, p.user_auction p_user_auction FROM auction as a right join products as p on a.product_id=p.product_id left join users as u on a.user_auction=u.user_id  WHERE a.user_auction=' + userId + ' group by a.product_id';

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.loadWonBiding = function(userId) {
	var deferred = Q.defer();

	var sql = 'SELECT * FROM products WHERE user_auction = ' + userId + ' AND ((price_present >= price_buy_now) OR (expired_date < NOW()))';

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.addMoreDescription = function(entity) {
	var deferred = Q.defer();

	var sql = mustache.render(
		'INSERT INTO product_description (product_id, product_description) VALUES ("{{productId}}", "{{{productDescription}}}")',
		entity
	);

	db.insert(sql).then(function(insertId) {
		if(insertId > 0) {
			deferred.resolve(insertId);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.kick = function(productId, userId) {
	var deferred = Q.defer();

	var sql = 'INSERT INTO kick (product_id, user_id) VALUES ("' + productId + '", "' + userId + '")';

	db.insert(sql).then(function(insertId) {
		if(insertId > 0) {
			deferred.resolve(insertId);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}