var Q = require('q');
var mustache = require('mustache');

var db = require('../fn/db.js');

exports.checkAccount = function(entity){
	var deferred = Q.defer();

	var sql = mustache.render(
		'SELECT username, email FROM users WHERE  username = "{{ username }}" OR email = "{{ email }}"',
		entity
	);

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			var row = rows[0];
			deferred.resolve(row);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.insert = function(entity) {
	var deferred = Q.defer();

	var sql = mustache.render(
		'INSERT INTO users (username, password, fullname, address, email) VALUES ("{{username}}", "{{password}}", "{{fullname}}", "{{address}}", "{{email}}")',
		entity
	);

	db.insert(sql).then(function(insertId) {
		deferred.resolve(insertId);
	});

	return deferred.promise;
}

exports.login = function(entity) {
	var deferred = Q.defer();

	var sql = mustache.render(
		'SELECT * FROM users as u left join permission as p on u.permission_id=p.permission_id WHERE u.is_active=1 AND u.username = "{{ username }}" AND u.password = "{{ password }}"',
		entity
	);

	console.log(entity);
	db.load(sql).then(function(rows) {
		console.log(rows);
		if(rows.length > 0) {
			var row = rows[0];
			var user = {
				id: row.user_id,
				username: row.username,
				fullname: row.fullname,
				email: row.email,
				permission_id: row.permission_id,
				permission_name: row.permission_name,
			};
			deferred.resolve(user);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.update = function(entity) {
	var deferred = Q.defer();

	if (entity.password == '') {
		var sql = mustache.render(
			'UPDATE users SET fullname = "{{ fullname }}", email = "{{ email }}" WHERE user_id = "{{ id }}"',
			entity
		);
	} else {
		var sql = mustache.render(
			'UPDATE users SET fullname = "{{ fullname }}", email = "{{ email }}", password = "{{ password }}" WHERE user_id = "{{ id }}"',
			entity
		);
	}

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			var user = {
				id: entity.id,
				fullname: entity.fullname,
				email: entity.email,
				// permission: 1
			};
			deferred.resolve(user);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.checkPassword = function(entity) {

	var deferred = Q.defer();

	var sql = mustache.render(
		'SELECT password FROM users WHERE user_id = "{{ id }}" AND password = "{{ password }}"',
		entity
	);

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			var row = rows[0];
			deferred.resolve(row);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.getUser = function(id) {
	var deferred = Q.defer();

	var sql = 'SELECT * FROM users WHERE user_id = ' + id;

	db.loadOne(sql).then(function(row) {
		if(row) {
			deferred.resolve(row);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.loadUserAuction = function(userAuctionId) {
	var deferred = Q.defer();

	var sql = 'SELECT * FROM users WHERE user_id = ' + userAuctionId;

	db.loadOne(sql).then(function(row) {
		if(row.length > 0) {
			deferred.resolve(row);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.getAll = function() {

	var deferred = Q.defer();

	var sql = 'SELECT * FROM users WHERE is_active=1';

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.deactive = function(id) {

	var deferred = Q.defer();

	var sql = 'UPDATE users SET is_active=0 WHERE user_id = ' + id;

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.resetPass = function(id, hashPass) {

	var deferred = Q.defer();

	var sql = 'UPDATE users SET password = "' + hashPass + '" WHERE user_id = ' + id;

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.upgrade = function(id) {

	var deferred = Q.defer();

	var sql = 'UPDATE users SET request_permission=1 WHERE user_id = ' + id;

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.getUpgradeAccount = function() {

	var deferred = Q.defer();

	var sql = 'SELECT * FROM users WHERE is_active=1 AND request_permission=1';

	db.load(sql).then(function(rows) {
		if(rows.length > 0) {
			deferred.resolve(rows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.confirmUpgradeAccount = function(id) {

	var deferred = Q.defer();

	var sql = 'UPDATE users SET request_permission=0, permission_id=3 WHERE user_id = ' + id;

	db.update(sql).then(function(changedRows) {
		if(changedRows > 0) {
			deferred.resolve(changedRows);
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}

exports.likeProduct = function(entity) {
	var deferred = Q.defer();

	var sql = 'SELECT favorite_products FROM users WHERE user_id = ' + entity.userId;

	db.loadOne(sql).then(function(row) {
		if(row) {
			var componentArr = (row.favorite_products).split(',');
			var isExist = componentArr.indexOf(entity.productId);
			if(isExist == -1) {
				var newFavorite = '';
				if(row.favorite_products == '') {
					newFavorite = row.favorite_products;
				} else {
					newFavorite = row.favorite_products + ',' + entity.productId;
				}
				var sqlUpdate = 'UPDATE users SET favorite_products="' + newFavorite + '" WHERE user_id=' + entity.userId;

				db.update(sqlUpdate).then(function(changedRows) {
					if(changedRows> 0) {
						deferred.resolve(changedRows);
					} else {
						deferred.resolve(null);
					}
				});
			} else {
				deferred.resolve(null);
			}
		} else {
			deferred.resolve(null);
		}
	});

	return deferred.promise;
}